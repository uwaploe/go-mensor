package mensor

import (
	"bytes"
	"testing"
)

func TestScanner(t *testing.T) {
	table := []struct {
		input, output string
	}{
		{input: "foo\r\n", output: "foo"},
		{input: "\x00read\r42.0\r\n", output: "read\r42.0"},
		{input: "\xc0read\r42.0\r\n", output: "read\r42.0"},
		{input: EOL, output: ""},
	}

	var rbuf bytes.Buffer
	s := newScanner(&rbuf, EOL)
	for _, e := range table {
		rbuf.Write([]byte(e.input))
		resp, _ := s.next()
		if resp != e.output {
			t.Errorf("Mismatch; expected %q, got %q", e.output, resp)
		}
	}

}
