// Package mensor provides an interface to Mensor serial pressure sensors. This
// package only supports the new command set (not the "legacy" commands).
package mensor

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

const (
	// Command terminator
	EOT = "\r"
	// Response terminator
	EOL = "\r\n"
)

// Command responses
const (
	ValidData      = "Ready"
	InvalidData    = "Invalid Data"
	InvalidCommand = "Unknown Command"
	PasswordReq    = "User Password Needed"
)

type PressureUnits int

// There are other units available, but these are the most standard.
const (
	UnitsPsi    PressureUnits = 1
	UnitsAtm                  = 13
	UnitsBar                  = 14
	UnitsMbar                 = 15
	UnitsKpa                  = 22
	UnitsPa                   = 23
	UnitsHpa                  = 35
	UnitsCustom               = 99
)

// respScanner parses the command responses from the device
type respScanner struct {
	rdr     io.Reader
	respBuf bytes.Buffer
	prompt  []byte
	tail    int
}

func newScanner(rdr io.Reader, prompt string) *respScanner {
	return &respScanner{
		rdr:    rdr,
		prompt: []byte(prompt),
	}
}

func (s *respScanner) next() (string, error) {
	s.respBuf.Reset()
	s.tail = -len(s.prompt)
	b := make([]byte, 1)
	for {
		_, err := s.rdr.Read(b)
		if err != nil {
			return s.respBuf.String(), err
		}

		s.respBuf.Write(b)
		s.tail++
		if s.tail < 0 {
			continue
		}

		if bytes.Equal(s.respBuf.Bytes()[s.tail:], s.prompt) {
			resp := strings.TrimSuffix(s.respBuf.String(), EOL)
			if len(resp) > 0 && (resp[0] == 0 || (resp[0]&0x80) == 0x80) {
				return resp[1:], nil
			} else {
				return resp, nil
			}
		}

	}

}

// MensorError represents an error message returned by the device.
type MensorError struct {
	src, text string
}

func (e *MensorError) Error() string {
	return fmt.Sprintf("%q response: %s", e.src, e.text)
}

type Device struct {
	port    io.ReadWriter
	scanner *respScanner
}

// New returns a new *Device instance attached to an RS-232 serial port
func New(rw io.ReadWriter) *Device {
	return &Device{
		port:    rw,
		scanner: newScanner(rw, EOL),
	}
}

func findError(req, resp string) error {
	if strings.HasPrefix(resp, InvalidCommand) || strings.HasPrefix(resp, InvalidData) {
		return &MensorError{
			src:  req,
			text: resp}
	}
	return nil
}

// Command sends a command to the device and returns the response along with
// any error that occurred.
func (d *Device) Command(req string) (string, error) {
	_, err := d.port.Write([]byte(req + EOL))
	if err != nil {
		return "", err
	}

	resp, err := d.scanner.next()
	if err == nil {
		err = findError(req, resp)
	}
	return resp, err
}

// Read a series of commands from a file and send to the device.
func (d *Device) Upload(cmdfile io.Reader) error {
	scanner := bufio.NewScanner(cmdfile)
	line := int(0)
	for scanner.Scan() {
		line++
		if scanner.Bytes()[0] == '#' {
			// Skip comment lines
			continue
		}
		cmd := strings.TrimRight(scanner.Text(), " \t\r\n")
		_, err := d.Command(cmd)
		if err != nil {
			return errors.Wrapf(err, "line %d", line)
		}
	}

	return scanner.Err()
}

// Return the sensor output in the current units.
func (d *Device) Pressure() (float64, error) {
	resp, err := d.Command("PRESS?")
	if err != nil {
		return 0, err
	}

	return strconv.ParseFloat(resp, 64)
}

// Set the pressure units
func (d *Device) SetUnits(units PressureUnits) error {
	_, err := d.Command(fmt.Sprintf("UNIT_INDEX %d", units))
	return err
}
